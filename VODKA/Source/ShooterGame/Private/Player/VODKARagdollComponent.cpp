#include "Player/VODKARagdollComponent.h"

#include "ShooterGame.h"

UVODKARagdollComponent::UVODKARagdollComponent()
	: Super()
	, TimeTillAutoGetUp(5.0f)
    , AutoGetUpRetryFrequency(0.1f)
	, PelvisBoneName(TEXT("b_Hips")) // TODO: Change pelvis bone name
	, BulletImpactMultiplier(700.0f) // 100
	, ExplosionImpactMultiplier(3000.0f) // 300
	, bIsRagdolled(false)
	, ServerPelvisLocationZeroBased(FVector_NetQuantize100::ZeroVector)
	, UnragdolLocationOffset(FVector(0,0,20))
	, OwnerCharacter(nullptr)
	, bIsReplicatingRagdoll(false)
	, bIsRecovering(false)
	, FreezeBodyConstraintHandle(nullptr)
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	PrimaryComponentTick.TickGroup = TG_PostPhysics;
	bAutoActivate = false;
	SetIsReplicatedByDefault(true);

	// Also make sure the freeze constraint handle starts with tick disabled
	if(FreezeBodyConstraintHandle)
	{
		FreezeBodyConstraintHandle->PrimaryComponentTick.bStartWithTickEnabled = false;
	}

	// Creation of a stiff physics constraint to hold the body in place when the server's ragdoll goes to sleep
	// in order to keep the desync'd client ragdoll from moving away from the correct position
	FreezeBodyConstraintHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("FreezeBodyConstraintHandle"));
	FreezeBodyConstraintHandle->LinearDamping = 200.f;
	FreezeBodyConstraintHandle->LinearStiffness = 10000.f;
	FreezeBodyConstraintHandle->AngularDamping = 500.f;
	FreezeBodyConstraintHandle->AngularStiffness = 1500.f;
	FreezeBodyConstraintHandle->InterpolationSpeed = 50.f;
	FreezeBodyConstraintHandle->bRotationConstrained = true;
	FreezeBodyConstraintHandle->SetActive(true);
	FreezeBodyConstraintHandle->Activate();
}

void UVODKARagdollComponent::OnRegister()
{
	// NOTE: ensure we obtain the owner soldier before calling Super::OnRegister to allow bAutoActivate to work
	OwnerCharacter = Cast<AShooterCharacter>(GetOwner());
	check(OwnerCharacter);

	Super::OnRegister();
}

void UVODKARagdollComponent::OnUnregister()
{
	Super::OnUnregister();

	OwnerCharacter = nullptr;
}

void UVODKARagdollComponent::Activate(bool bReset)
{
	RagdollPawn();
}

void UVODKARagdollComponent::Deactivate()
{
	UnragdollPawn();
}

void UVODKARagdollComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// If we're recovering from ragdoll, run the code for that on both the server and clients
	if (bIsRecovering)
	{
		TickRecovery(DeltaTime);
	}
	else if (bIsReplicatingRagdoll) // This is the branch for ragdoll replication corrections
	{
		// Set the capsule's position on the Pelvis so that we can use the capsule for collision detection and other things
		OwnerCharacter->GetCapsuleComponent()->SetWorldLocation(OwnerCharacter->GetMesh()->GetSocketLocation(PelvisBoneName), false, nullptr, ETeleportType::TeleportPhysics);

		if (HasServerFunctionality(GetOwner()))
		{
			TickServerRepCorrection();
		}
	}
}

void UVODKARagdollComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UVODKARagdollComponent, bIsRagdolled);
	DOREPLIFETIME(UVODKARagdollComponent, ServerPelvisLocationZeroBased);
	//DOREPLIFETIME(UVODKARagdollComponent, bBodyHasSettled);
}

bool UVODKARagdollComponent::IsInRagdoll() const
{
	return bIsRagdolled || bIsRecovering;
}

void UVODKARagdollComponent::ServerSetIsRagdolled_Implementation(bool bNewIsRagdolled)
{
	SetIsRagdolled(bNewIsRagdolled);
}

bool UVODKARagdollComponent::ServerSetIsRagdolled_Validate(bool bNewIsRagdolled)
{
	return true;
}

void UVODKARagdollComponent::SetIsRagdolled(bool bNewIsRagdolled)
{
	if (IsRegistered() == false)
	{
		return;
	}

	USkeletalMeshComponent* CharacterMesh = OwnerCharacter->GetMesh();
	check(CharacterMesh);

	//No reason to try to set the state again if they are the same.
	if (bNewIsRagdolled == bIsRagdolled)
	{
		return;
	}
	
	// CLIENT: Send state to the server
	if (!HasServerFunctionality(GetOwner()))
	{
		ServerSetIsRagdolled(bNewIsRagdolled);
	}
	// SERVER: set a timer to get the soldier up again, changes state
	else
	{
		if(bNewIsRagdolled)
		{
			// Set the auto get up timer
			GetWorld()->GetTimerManager().SetTimer(AutoGetupTimer, this, &UVODKARagdollComponent::AutoGetUp, AutoGetUpRetryFrequency, true, TimeTillAutoGetUp);

			// Note: this code also runs for ReplicatedAutoGetUp
			// Set ServerPelvisLocationZeroBased for the first time on server
			ServerPelvisLocationZeroBased = FRepMovement::RebaseOntoZeroOrigin(CharacterMesh->GetBoneLocation(PelvisBoneName), GetWorld()->OriginLocation);
		}
		else
		{
			CharacterMesh->SetIsReplicated(false);
		}
		

		// Run the ragdoll on the server first with the new state and then change the state (with replication)
		RagdollFromState(bNewIsRagdolled);
		bIsRagdolled = bNewIsRagdolled;
	}
}

void UVODKARagdollComponent::OnRep_IsRagdolled()
{
	RagdollFromState(bIsRagdolled);
}

void UVODKARagdollComponent::AutoGetUp()
{
	SetIsRagdolled(false);
}

void UVODKARagdollComponent::RagdollFromState(bool bNewIsRagdolled)
{
	if(bNewIsRagdolled)
	{
		// Activate ticking for replicated ragdolls
		SetComponentTickEnabled(true);

		// Stop the movement component from processing falling damage
		OwnerCharacter->GetCharacterMovement()->DisableMovement();

		// Set ragdoll only if we're not already coming from a replicated ragdoll state (so if the player gives up we don't get a ragdoll nudge)
		if (!bIsReplicatingRagdoll)
		{
			RagdollPawn();
		}
		bIsReplicatingRagdoll = true;
	}
	else
	{
		// deactivate ticking
		SetComponentTickEnabled(false);

		// restore the movement component
		OwnerCharacter->GetCharacterMovement()->SetMovementMode(MOVE_Walking);

		bIsReplicatingRagdoll = false;
		UnragdollPawn();
	}
}

void UVODKARagdollComponent::RagdollPawn()
{
	USkeletalMeshComponent* SoldierMesh = OwnerCharacter->GetMesh();
	UCapsuleComponent* SoldierCapsule = OwnerCharacter->GetCapsuleComponent();
	UCharacterMovementComponent* SoldierMovement = OwnerCharacter->GetCharacterMovement();
	check(SoldierMesh && SoldierCapsule && SoldierMovement);

	// Set capsule collision and physics off
	SoldierCapsule->SetSimulatePhysics(false);
	SoldierCapsule->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SoldierCapsule->SetCollisionResponseToAllChannels(ECR_Ignore);
	SoldierCapsule->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);

	// Mesh is being replicated from the server where it is invisible, 
	// make it visible for everyone except the player who is ragdolling
	if (GetOwnerRole() != ROLE_AutonomousProxy)
	{
		SoldierMesh->SetVisibility(true);
	}

	// Unset the base and enable collision on this actor
	OwnerCharacter->SetBase(nullptr);

	// TODO: verify that these collision profiles exist
	// Change the mesh collision settings to be a ragdoll
	static FName RagdollProfile(TEXT("Ragdoll"));
	SoldierMesh->SetCollisionProfileName(RagdollProfile);

	// enable CCD on root body to avoid passing through surfaces at high velocity/low framerate (let the other bones clip for perf)
	if (SoldierMesh->GetBodyInstance() != nullptr)
	{
		SoldierMesh->GetBodyInstance()->bUseCCD = true;
	}

#if 1  // TODO: not needed if we use phys constraint
	// Set the Mesh as the root component to let it drive the actor and replicate the physics
	if (OwnerCharacter->GetRootComponent() != SoldierMesh)
	{
		SoldierCapsule->RemoveFromRoot();
		SoldierCapsule->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		SoldierMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		OwnerCharacter->SetRootComponent(SoldierMesh);
	}
#endif

	// Disable physics on the capsule or it may fall down to infinity and beyond
	SoldierCapsule->SetSimulatePhysics(false);

	// Simulate physics on the skeleton mesh
	SoldierMesh->SetAllBodiesPhysicsBlendWeight(1.0f);
	SoldierMesh->SetAllBodiesSimulatePhysics(true);

	// Apply accumulated forces on character movement
	SoldierMovement->ApplyAccumulatedForces(0.0f);

	// TODO: fix this as it stops all momentum
	SoldierMesh->SetAllPhysicsLinearVelocity(SoldierMovement->Velocity, false);

	// Stop movement
	SoldierMovement->StopMovementImmediately();
	SoldierMovement->Velocity = FVector::ZeroVector;

	// Apply impulse on the bone that got hit (only if the soldier hasn't been ejected from the vehicle)
	FTakeHitInfo& LastTakeHitInfo(OwnerCharacter->GetLastTakeHitInfo());
	switch (LastTakeHitInfo.GetDamageEvent().GetTypeID())
	{
	case FPointDamageEvent::ClassID:
		{
			FPointDamageEvent const* const PointDamageEvent = (FPointDamageEvent*)(&LastTakeHitInfo.GetDamageEvent());// (FPointDamageEvent*)(&OwnerSoldier->LastTakeHitInfo.GetDamageEvent());
			if (PointDamageEvent && PointDamageEvent->HitInfo.BoneName != NAME_None)
			{
				const FName BoneName = PointDamageEvent->HitInfo.BoneName;
				const FVector DamageImpulse = PointDamageEvent->ShotDirection.GetSafeNormal() * BulletImpactMultiplier * PointDamageEvent->Damage;
				SoldierMesh->AddImpulseAtLocation(DamageImpulse, PointDamageEvent->HitInfo.ImpactPoint, BoneName);
			}
			break;
		}
	case FRadialDamageEvent::ClassID:
		{
			FRadialDamageEvent const* const RadialDamageEvent = (FRadialDamageEvent*)(&LastTakeHitInfo.GetDamageEvent());
			if (RadialDamageEvent)
			{
				const FVector DamageImpulseDirection = SoldierMesh->GetSkeletalCenterOfMass() - RadialDamageEvent->Origin;
				const float DamageValue = RadialDamageEvent->Params.GetDamageScale(DamageImpulseDirection.Size()) * ExplosionImpactMultiplier * RadialDamageEvent->Params.BaseDamage;
				SoldierMesh->AddRadialImpulse(RadialDamageEvent->Origin, RadialDamageEvent->Params.GetMaxRadius(), DamageValue, ERadialImpulseFalloff::RIF_Constant);
			}
			break;
		}
	default:
		break;
	}

	// We don't want desync between the 1p and 3p so we hide the upper mesh while we are in ragdoll
	USkeletalMeshComponent* SoldierUpperMesh = OwnerCharacter->GetMesh1P();
	if (IsValid(SoldierUpperMesh))
	{
		SoldierUpperMesh->SetHiddenInGame(true);
	}

	// TODO: this might be important to ser the correct anim state
	// Set the ragdoll flag on the anim instance
	//OwnerSoldier->CachedAnimInstance3p->bIsSoldierRagdolled = true;
}

void UVODKARagdollComponent::UnragdollPawn()
{
	USkeletalMeshComponent* SoldierMesh = OwnerCharacter->GetMesh();
	UCapsuleComponent* SoldierCapsule = OwnerCharacter->GetCapsuleComponent();
	UCharacterMovementComponent* SoldierMovement = OwnerCharacter->GetCharacterMovement();
	check(SoldierMesh && SoldierCapsule && SoldierMovement);

	// Release the mesh from the constraint handle
	FreezeBodyConstraintHandle->ReleaseComponent();

	// Restore animations
	SoldierMesh->bPauseAnims = false;

	// Rotate the capsule into the orientation of the controller so that they match
	AShooterPlayerController* PC = IsValid(OwnerCharacter->Controller) ? Cast<AShooterPlayerController>(OwnerCharacter->Controller) : nullptr;
	FRotator FixedRotation = SoldierCapsule->GetRelativeRotation();
	FixedRotation.Pitch = FixedRotation.Roll = 0.0f;
	if (IsValid(PC))
	{
		// always recover in the direction the controller is facing since turning is instant
		FixedRotation.Yaw = PC->GetControlRotation().Yaw;
	}
	SoldierCapsule->SetRelativeRotation(FixedRotation);

#if 0 // TODO: not needed if we use phys constraint
	// Force the bRepPhysics to false and immediately remove the mesh from the PhysicsReplication list of components to target
	// so that we avoid lerping between old values/states
	OwnerCharacter->GetReplicatedMovement().bRepPhysics = false;
	if (UWorld* World = GetWorld())
	{
		if (FPhysScene* PhysScene = World->GetPhysicsScene())
		{
			if (FPhysicsReplication* PhysicsReplication = PhysScene->GetPhysicsReplication())
			{
				UPrimitiveComponent* RootPrimComp = Cast<UPrimitiveComponent>(OwnerSoldier->GetRootComponent());
				if (RootPrimComp)
				{
					PhysicsReplication->RemoveReplicatedTarget(RootPrimComp);
				}
			}
		}
	}
#endif

	// Reset the physics state for the mesh
	const AShooterCharacter* DefaultSoldier = OwnerCharacter->GetClass()->GetDefaultObject<AShooterCharacter>();
	SoldierMesh->PutAllRigidBodiesToSleep();
	SoldierMesh->VisibilityBasedAnimTickOption = DefaultSoldier->GetMesh()->VisibilityBasedAnimTickOption;
	SoldierMesh->bBlendPhysics = false;
	SoldierMesh->SetAllBodiesPhysicsBlendWeight(0.0f);
	SoldierMesh->SetAllBodiesNotifyRigidBodyCollision(false);
	SoldierMesh->SetAllBodiesSimulatePhysics(false);
	SoldierMesh->SetSimulatePhysics(false);

	// TODO: verify that these collision profiles exist
	// Simulate body is now set, we can now modify the collision profile safely
	OwnerCharacter->SetActorEnableCollision(true);
	static FName CollisionProfileName(TEXT("CharacterMesh"));
	SoldierMesh->SetCollisionProfileName(CollisionProfileName);
	static FName CapsuleCollisionProfileName(TEXT("Pawn"));
	SoldierCapsule->SetCollisionProfileName(CapsuleCollisionProfileName);
	SoldierCapsule->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	// Set the capsule as the root component again (if it isn't already) and revert back to the hierarchy before ragdolling
	if (OwnerCharacter->GetRootComponent() != SoldierCapsule)
	{
		SoldierMesh->RemoveFromRoot();
		SoldierMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		SoldierCapsule->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		OwnerCharacter->SetRootComponent(SoldierCapsule);
		OwnerCharacter->GetMovementComponent()->SetUpdatedComponent(OwnerCharacter->GetRootComponent());
	}

#if 1 // TODO: not needed if we use phys constraint
	// We are finally done resetting location/rotation/physics so we can now reattach the mesh to the capsule
	const FVector OriginalRelativeLocation = OwnerCharacter->GetBaseTranslationOffset()/* + OwnerCharacter->ShiftMeshOffset*/;
	const USkeletalMeshComponent* DefaultMesh = DefaultSoldier->GetMesh();
	SoldierMesh->AttachToComponent(OwnerCharacter->GetRootComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale);
	SoldierMesh->SetRelativeLocationAndRotation(OriginalRelativeLocation, DefaultMesh->GetRelativeRotation());
	SoldierMesh->SetRelativeScale3D(DefaultSoldier->GetMesh()->GetRelativeScale3D());

	// Restore the first person mesh
	OwnerCharacter->GetMesh1P()->AttachToComponent(OwnerCharacter->GetRootComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale);
#endif

	// Try finding a position for the soldier's capsule, if it can't find a valid position, force the server position but with a shift to prevent spawning below ground
	FVector AdjustedLoc = FRepMovement::RebaseOntoLocalOrigin(ServerPelvisLocationZeroBased, GetWorld()->OriginLocation);
	bool bFoundValidLoc = FindValidUnragdollPosition(AdjustedLoc);
	OwnerCharacter->SetActorLocation(bFoundValidLoc ? AdjustedLoc : AdjustedLoc + UnragdolLocationOffset);

	// TODO: might need this
	// Set the ragdoll flag back on the anim instance
	//OwnerCharacter->CachedAnimInstance3p->bIsSoldierRagdolled = false;

	USkeletalMeshComponent* SoldierUpperMesh = OwnerCharacter->GetMesh1P();
	if (IsValid(SoldierUpperMesh))
	{
		SoldierUpperMesh->SetHiddenInGame(false);
	}

	if (HasServerFunctionality(GetOwner()))
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoGetupTimer);
	}
}

void UVODKARagdollComponent::TickServerRepCorrection()
{
	// Function only runs on a server and if ragdoll is replicating
	check(HasServerFunctionality(GetOwner()) && bIsReplicatingRagdoll);

	// The server only needs to send the Pelvis location to the clients if it changed
	const FVector NewServerPelvisLocationZeroBased = FRepMovement::RebaseOntoZeroOrigin(OwnerCharacter->GetMesh()->GetBoneLocation(PelvisBoneName), GetWorld()->OriginLocation);
	if(NewServerPelvisLocationZeroBased.Equals(ServerPelvisLocationZeroBased) == false)
	{
		ServerPelvisLocationZeroBased = NewServerPelvisLocationZeroBased;
	}
}

void UVODKARagdollComponent::TickClientRepCorrection(float DeltaTime)
{
	// Function only runs on clients and if ragdoll is replicating
	check(this->GetOwnerRole() != ROLE_Authority && bIsReplicatingRagdoll);

	USkeletalMeshComponent* CharacterMesh = OwnerCharacter->GetMesh();
	check(CharacterMesh);

#if 0
	// Pause animations to prevent ragdoll from having weird movements (only for players who aren't in the spawn screen)
	// Note: this is done here to prevent a strange bug where newly spawned players see ragdolls stuck in a pose
	ASQPlayerController* PC = GetWorld()->GetFirstPlayerController<ASQPlayerController>();
	if (IsValid(PC) && PC->IsPlayer() && SoldierMesh->bPauseAnims == false && bIsPelvisFrozen)
	{
		SoldierMesh->bPauseAnims = true;
	}
#endif

	
}

void UVODKARagdollComponent::TickRecovery(float DeltaTime)
{
	unimplemented();
}

bool UVODKARagdollComponent::FindValidUnragdollPosition(FVector& OutNewSpawnLocation)
{
	UWorld* World = GetWorld();
	UCapsuleComponent* SoldierCapsule = OwnerCharacter->GetCapsuleComponent();
	check(World && SoldierCapsule);

	// Set capsule location as the proposed valid position to unragdoll
	const FVector CapsuleLocation = SoldierCapsule->GetComponentLocation();
	OutNewSpawnLocation = CapsuleLocation;

	// Gather child actors to be ignored by traces
	TArray<AActor*> ChildActors;
	OwnerCharacter->GetAllChildActors(ChildActors);

	// Start at the capsule/pelvis height and trace down towards the floor (floor spawning)
	FHitResult OutHit;
	const FVector CapsuleHalfHeightVec(0, 0, SoldierCapsule->GetUnscaledCapsuleHalfHeight());
	static const FName UnragdollTraceName = FName(TEXT("UnragdollTrace")); // TODO: does this trace profile exist?
	FCollisionQueryParams TraceParams(UnragdollTraceName, true, OwnerCharacter);
	TraceParams.AddIgnoredActors(ChildActors);
	bool bFloorBlockingHit = World->LineTraceSingleByChannel(OutHit, CapsuleLocation, CapsuleLocation - CapsuleHalfHeightVec, ECC_Visibility, TraceParams);
	if (bFloorBlockingHit)
	{
		// Now trace from a height that allows space for the capsule and see if we hit anything else
		// this will pass for sloped terrain but fail for vehicles and other static world stuff
		FHitResult OtherOutHit;
		bool bAnyOtherBlockingHit = World->LineTraceSingleByChannel(OtherOutHit, OutHit.Location + 2*CapsuleHalfHeightVec, CapsuleLocation, ECC_Visibility, TraceParams);
		if (bAnyOtherBlockingHit == false && OutHit.Location.ContainsNaN() == false)
		{
			// The capsule's location is at its centre so add the half height to the ground hit location
			const FVector& SpawnLocation = OutHit.Location + CapsuleHalfHeightVec;

			// Now that we know there's nothing in that line trace, let's check the more expensive capsule overlap query
			// This will tell us exactly if that capsule can exist at the proposed location without colliding with anything
			if (SoldierCapsule->IsQueryCollisionEnabled())
			{
				// Prepare the overlap query
				TArray<FOverlapResult> Overlaps;
				ECollisionChannel const BlockingChannel = SoldierCapsule->GetCollisionObjectType();
				FCollisionShape const CollisionShape = SoldierCapsule->GetCollisionShape();
				FCollisionQueryParams Params(UnragdollTraceName, false, OwnerCharacter);
				FCollisionResponseParams ResponseParams;
				SoldierCapsule->InitSweepCollisionParams(Params, ResponseParams);
				Params.AddIgnoredActors(ChildActors);
				const FQuat& SpawnRotation = SoldierCapsule->GetComponentQuat();

				// Find the overlaps
				FVector ProposedAdjustment = FVector::ZeroVector;
				bool bFoundBlockingHit = World->OverlapMultiByChannel(Overlaps, SpawnLocation, SpawnRotation, BlockingChannel, CollisionShape, Params, ResponseParams);
				if (bFoundBlockingHit)
				{
					// If there are overlaps at this point they will likely not be too important since they don't block the vertical line trace at the centre of the capsule
					// Most likely they can be terrain slopes or small objects and all we need to do is push the capsule location a bit towards a place where it can exist without collisions
					FMTDResult MTDResult;
					for (int32 HitIdx = 0; HitIdx < Overlaps.Num(); HitIdx++)
					{
						UPrimitiveComponent* const OverlapComponent = Overlaps[HitIdx].Component.Get();
						if (OverlapComponent && OverlapComponent->GetCollisionResponseToChannel(BlockingChannel) == ECR_Block)
						{
							// If this overlap component is a blocker for the capsule, calculate the penetration and add it to the proposed adjustment
							bool bSuccess = OverlapComponent->ComputePenetration(MTDResult, CollisionShape, SpawnLocation, SpawnRotation);
							if (bSuccess)
							{
								ProposedAdjustment += MTDResult.Direction * MTDResult.Distance;
							}
						}
					}
				}

				// Add the proposed adjustment to the initially discovered location and return successfully
				OutNewSpawnLocation = SpawnLocation + ProposedAdjustment;
				return true;
			}
			else
			{
				// TODO: error
			}
		}
	}

	return false;
}
