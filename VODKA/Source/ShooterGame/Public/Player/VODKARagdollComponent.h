#pragma once

#include "Components/ActorComponent.h"

#include "VODKARagdollComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), Config=Game)
class UVODKARagdollComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Initial time until we start trying to get up in seconds
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    float TimeTillAutoGetUp;

	// Frequency of checking if the body can get up in seconds
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    float AutoGetUpRetryFrequency;

	UPROPERTY(EditAnywhere)
	FName PelvisBoneName;

	// The multiplier for bullet impact impulses on the ragdolls
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    float BulletImpactMultiplier;

	// The multiplier for explosion impact impulses on the ragdolls
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    float ExplosionImpactMultiplier;
	
    UVODKARagdollComponent();

	virtual void OnRegister() override;
	virtual void OnUnregister() override;

	virtual void Activate(bool bReset=false) override;
	virtual void Deactivate() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Returns whether or not this soldier is in ragdoll or not. */
	UFUNCTION(BlueprintPure, Category = "SQ|Ragdoll")
    bool IsInRagdoll() const;

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "SQ|Ragdoll")
    virtual void ServerSetIsRagdolled(bool bNewIsRagdolled);

	UFUNCTION(BlueprintCallable, Category = "SQ|Ragdoll")
    virtual void SetIsRagdolled(bool bNewIsRagdolled);

protected:
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_IsRagdolled)
    uint8 bIsRagdolled : 1;
	
	// Zero-based server-side location for the pelvis
	UPROPERTY(BlueprintReadOnly, Replicated)
	FVector_NetQuantize100 ServerPelvisLocationZeroBased;

	UPROPERTY(Config)
    FVector UnragdolLocationOffset;

	FTimerHandle AutoGetupTimer;

	UPROPERTY()
    class AShooterCharacter* OwnerCharacter;

	bool bIsReplicatingRagdoll;
	bool bIsRecovering;

	UPROPERTY()
    class UPhysicsHandleComponent* FreezeBodyConstraintHandle;

	UFUNCTION()
    virtual void OnRep_IsRagdolled();

private:
	// Timer functions
	void AutoGetUp();

	void RagdollFromState(bool bNewIsRagdolled);
	void RagdollPawn();
	void UnragdollPawn();

	// Tick functions
	void TickServerRepCorrection();
	void TickClientRepCorrection(float DeltaTime);
	void TickRecovery(float DeltaTime);

	// Auxiliary capsule placement function
	virtual bool FindValidUnragdollPosition(FVector& OutNewSpawnLocation);
};